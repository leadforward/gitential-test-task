import * as React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import AppMenu from 'components/AppMenu/AppMenu'
import Home from 'components/Home/Home'
import Login from 'components/Login/Login'

const Dashboard = React.lazy(() => import('./components/Dashboard/Dashboard'))

const withSuspense = Component => {
  return props => (
    <React.Suspense fallback={<div>Loading...</div>}>
      <Component {...props} />
    </React.Suspense>
  )
}

export default () => {
  return (
    <Router>
      <AppMenu />
      <Route path="/" exact component={Home} />
      <Route path="/login" component={Login} />
      <Route path="/dashboard" component={withSuspense(Dashboard)} />
    </Router>
  )
}
