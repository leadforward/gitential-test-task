import * as React from 'react'
import {view} from '@risingstack/react-easy-state'
import Layout from '../Layout/Layout'
import authStore from "~stores/authStore";
import {useEffect} from "preact/hooks";

export default view(() => {

    const {checkLoginState} = authStore

    useEffect(() => {
        (async () => await checkLoginState())()
    });

    return (
        <Layout>
            <div className="section">
                <h2 className="title is-2">Home Page</h2>

                <p>This page is public, but to access the Dashboard page you have to provide the secret password. (Hint:
                    123Pass123)</p>

                <p>After you logged in you have 15 seconds while you can load the Dashboard page, after that time if you
                    want to access it again, then you have to login again.</p>
            </div>
        </Layout>
    )
})
