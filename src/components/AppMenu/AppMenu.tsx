import * as React from 'react'
import { Route, Link } from 'react-router-dom'

import * as css from './AppMenu.scss'
import {useState} from "preact/hooks";

const AppMenu = () => {
    const [menuOpened, setMenuOpened] = useState(false);

    return (
        <nav className="navbar" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
            <span className="navbar-item">
                Gitential Test Task
            </span>

                <a role="button"
                   className={`navbar-burger burger ${menuOpened ? `${css['is-active']} is-active` : ''}`}
                   aria-label="menu"
                   aria-expanded="false"
                   data-target="navbarBasicExample"
                   onClick={() => setMenuOpened(!menuOpened)}
                >
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div id="navbarBasicExample" className={`navbar-menu ${menuOpened ? `${css['is-active']} is-active` : ''}`}>
                <div className={`navbar-end ${css.menuItems}`}>
                    <ListItemLink label="Home" to="/" className="navbar-item" />
                    <ListItemLink label="Dashboard [Protected]" to="/dashboard" className="navbar-item" />
                </div>
            </div>
        </nav>
    )
}

const ListItemLink = ({ label, to, ...rest }) => (
  <Route
    path={to}
    children={({ match }) => (
        <Link to={to} {...rest} className={match ? 'active' : ''}>
            {label}
        </Link>
    )}
  />
)

export default AppMenu
