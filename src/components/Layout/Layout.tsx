import * as React from 'react'

export default (props) => (
    <div className="section">
        <div className="container">
            <div className="columns is-centered">
                <div className="column is-8 has-text-centered">

                    { props.children }

                </div>
            </div>
        </div>
    </div>
)
