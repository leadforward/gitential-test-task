import * as React from 'react'
import Layout from "~components/Layout/Layout";
import {useEffect} from "preact/hooks";
import authStore from "~stores/authStore";
import { Redirect } from 'react-router-dom'
import {view} from "@risingstack/react-easy-state";
import CanvasJSReact from '~components/CanvasJS/canvasjs.react';
const CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default view(() => {
    const { checkLoginState, isLoggedIn, logout } = authStore

    useEffect(() => {
        (async () => await checkLoginState())()
    });

    const options = {
        exportEnabled: true,
        animationEnabled: true,
        title: {
            text: "Other people's responses to the fact that I am a software engineer"
        },
        data: [{
            type: "pie",
            startAngle: 75,
            toolTipContent: "<b>{label}</b>: {y}%",
            showInLegend: "true",
            legendText: "{label}",
            indexLabelFontSize: 16,
            indexLabel: "{label} - {y}%",
            dataPoints: [
                { y: 5, label: "You must be good with numbers, huh?" },
                { y: 85, label: "So you can fix my computer, right?" },
                { y: 7, label: "Oh, I have a [insert relative here] that does that" },
                { y: 3, label: "Wow, that sounds hard" },
            ]
        }]
    }

    return (
        <>
            {!isLoggedIn ? <Redirect to="/login" /> : null}

            <Layout>
                <h2 className="title is-2">Protected Contact Page</h2>

                <p>Just try to log out and you can't return to this page...</p>

                <div className="section">
                    <button className="button is-danger" onClick={logout}>Log out</button>
                </div>

                <div className="section">
                    <CanvasJSChart options = {options} />
                </div>
            </Layout>
        </>
    )
})
