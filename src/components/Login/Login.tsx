import * as React from 'react'
import {view} from '@risingstack/react-easy-state'
import authStore from 'stores/authStore'
import Layout from '../Layout/Layout'
import {useState} from "preact/hooks"
import * as css from './Login.scss'
import { Redirect } from 'react-router-dom'

export default view(() => {
    const { login, isLoading, setToken, isLoggedIn, checkLoginState } = authStore

    const [formFields, setFormFields] = useState({
        password: {
            value: '',
            touched: false,
            valid: true,
        }
    })

    const onSubmit = async event => {
        event.preventDefault()

        if (isLoading) {
            return
        }

        const loginData = Object.keys(formFields).reduce((allFields, field) => {
            allFields[field] = formFields[field].value
            return allFields
        }, {} as { password: string })

        const { res, error } = await login(loginData)
        if (error) {
            console.error(error)
        } else if ('token' in res) {
            await setToken(res.token)
            await checkLoginState()
        }
    }

    const checkValidity = (inputIdentifier: string, value: string) => {
        switch (inputIdentifier) {
            case 'password':
                return value.length > 5
            default:
                return false
        }
    }

    const isFormValid = () => {
        let valid = true
        for (let field in formFields) {
            valid = valid && checkValidity(field, formFields[field].value)
            if (!valid) {
                break
            }
        }
        return valid
    }

    const onInputChanged = (event, inputIdentifier) => {
        setFormFields({
            ...formFields,
            [inputIdentifier]: {
                value: event.target.value,
                touched: true,
                valid: checkValidity(inputIdentifier, event.target.value),
            }
        })

    }

    return (
        <>
            {isLoggedIn ? <Redirect to="/dashboard" /> : null}

            <Layout>
                <h2 className="title is-2">Login Page</h2>

                <form className={`card ${css.form}`} onSubmit={onSubmit}>
                    <div className="card-content">
                        <div className="field">
                            <p className={`control ${css.formControl}`}>
                                <input className={`input
                                        ${!formFields.password.valid ? 'is-danger' : ''}
                                        ${formFields.password.valid && formFields.password.touched ? 'is-success' : ''}
                                    `}
                                       type="password"
                                       placeholder="Password"
                                       value={formFields['password'].value}
                                       onChange={(event) => onInputChanged(event, 'password')}
                                />
                            </p>
                        </div>

                        <div className="field">
                            <p className={`control ${css.formControl}`}>
                                {isLoading && (`Logging in...`)}
                                {!isLoading && (
                                    <input className="button is-success"
                                           type="submit"
                                           value="Login"
                                           disabled={!isFormValid()}
                                    />
                                )}
                            </p>
                        </div>
                    </div>
                </form>
            </Layout>
        </>
    )
})
