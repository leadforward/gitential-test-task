import { store } from '@risingstack/react-easy-state'
import {reqGet, reqPost, setToken} from 'utils/Req'

const _store = store({
  isLoading: false,

  isLoggedIn: false,

  async login({ password }) {
    _store.isLoading = true
    const response = await reqPost('/login', { password })
    _store.isLoading = false
    return response
  },

  async checkLoginState() {
    _store.isLoading = true
    const { res } = await reqGet('/is-logged-in')
    _store.isLoading = false
    _store.isLoggedIn = res === 'ok'
  },

  setToken,

  async logout() {
    await setToken(null)
    _store.isLoggedIn = false
  },
})

export default _store
