const jwt = require('jsonwebtoken')

exports.handler = async request => {
    const loginData = JSON.parse(request.body);

    const secretPassword = process.env.SECRET_PASSWORD
    const salt = process.env.SECRET_SALT || 'bfh3oi76rfgyiu5nr'

    if (!('password' in loginData) || typeof loginData.password !== 'string') {
        return {
            statusCode: 400,
            body: 'Type of password is not correct, it should be a string'
        }
    }

    if (loginData.password !== secretPassword) {
        return {
            statusCode: 401,
            body: 'Incorrect credentials'
        }
    }

    const token = jwt.sign(loginData, salt, { expiresIn: '15s' })

    return {
        statusCode: 200,
        body: JSON.stringify({
            token
        })
    };
};
