const jwt = require('jsonwebtoken')

exports.handler = async request => {
    const salt = process.env.SECRET_SALT || 'bfh3oi76rfgyiu5nr'

    try {
        const token = 'authorization' in request.headers ? request.headers.authorization.split(' ')[1] : null;

        if (!token) {
            return {
                statusCode: 401,
                body: JSON.stringify('Not authenticated')
            };
        }

        const decodedToken = jwt.verify(token, salt)

        if (!decodedToken || Date.now() >= decodedToken.exp * 1000) {
            return {
                statusCode: 401,
                body: JSON.stringify('Not authenticated')
            }
        }

    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify({
                error
            })
        };
    }


    return {
        statusCode: 200,
        body: JSON.stringify('ok')
    };

};
