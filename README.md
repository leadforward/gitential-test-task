# Gitential Test Task

Parcel + Typescript + React/Preact + Router + CSS Modules + SASS + Netlify functions

And more:

- Lazy loading (code splitting) for a page (Route).
- CSS scoping using CSS Modules: just import a local CSS or SASS file and use it.
- Store management with react-easy-state (easier than Redux, MobX).
- Making Ajax requests using Whatwg-fetch Req.

### Demo
A working demo available at: https://blissful-kalam-007227.netlify.app/

### 🔧 Installation

Clone this project:

```
git clone https://gitlab.com/leadforward/gitential-test-task.git your-app
cd your-app
rm -rf .git
npm install
```

- Upload this code into your own repository and create a Netlify site from it
- Install Netlify CLI: `npm install -g netlify-cli`
- Authenticate with Netlify `netlify login`
- Then you can run `netlify dev` then open http://localhost:8888/

This will emulate the API endpoints in local. Please note that if you try to load the site on the standard 1234 Parcel port, then you'll have CORS errors, because the Netlify endpoints are only accessible on 8888.

You can specify the required password by placing it in the .env file inside the project's root directory:

```
SECRET_PASSWORD=123Pass123
```

If you create this site in you own Netlify account, then you also need to set this environment variable inside Netlify's dashboard. 

### ⚙️ Commands

```
netlify dev           launch DEV mode takes 1.4s (tsc watch, parcel and Netlify)
npm run build         build for PROD to static directory ./dist
```

### CSS

- SCSS is recommended, but optional. You can also use ".css" files normally.

### Others

Recommend:

- VSCode & prettier addon
